# Esphome Configs

ESPHome configuration files for my projects. Some of these may be unfinished or have issues.

### Light Info Panel

I will explain this... later

### Home assistant configuration file

This is a file used by the projects here to generate things used by the ESPs, it will be needed for some projects and usually just creates a more user friendly output. You should add the parts you need to `config/configuration.yaml` in Home assistant
